การติดตั้งระบบ
1.โปรแกรมนี้จะใช้ anaconda ในการใช้งานโดยใช้งานในส่วน ของ JupyterLab ซึ่งจะรันโปรแกรมที่ web browser
2. ในตัวคอมพิวเตอร์ที่ติดตั้งจะต้องใช้งานภาษาเหล่านี้ได้ทั้ง Python 3.9 HTML JS CSS 
3.ต้องมี library ที่มี versionขั้นต่ำ ดังต่อไปนี้ใน anaconda 
•	json
•	propy3 version 1.1.1.1
•	pandas version 1.4.4
•	numpy version 1.21.5
•	joblib version 1.2.0
•	scikeras version 0.10.0
•	keras version 2.12.0
•	Flask version 1.1.2
4.ตัว File จะต้องอยู่ใน C:\Users\”Username”\  โปรแกรม anaconda ในส่วน ของ JupyterLab จึงจะเจอตัว  folder ของโปรแกรม

วิธีการใช้งานระบบ
1.ในการใช้งานต้องไปที่ app.ipynb และรันโปรแกรมทุกส่วน เมื่อรันเสร็จจะมีเว็บ http://localhost:8081/ปรากฎขึ้นมาตรงท้ายของส่วน output 
2.กดเข้าไป web browser ก็จะแสดงหน้าเว็บแอพพลิเคชั่น ในส่วนเว็บแอพพลิเคชั่นจะมี 3 ส่วน คือ input , Venn diagram , ตาราง 
3.ใส่ Sequence peptide ที่ต้องการทำนายโดย
•	ตัว input จะมีตัว Sequence peptide ได้มากสุด 50 peptide
•	ตัว input ต้องแบ่งตัว Sequence peptide ด้วย comma (,)
•	ตัว Sequence peptide จะต้องมีขนาดอยู่ที่ 2-50 กรดอะมิโน
•	ตัว Sequence peptide จะต้องเป็นกรดอะมิโนหลัก 20 ชนิดเท่านั้น
4.กด Submit เพื่อทำนาย ตัวเว็บแอพพลิเคชั่นจะแสดง output ในส่วนของ Venn diagram 
5.ในส่วนของตารางกดปุ่มเพื่อเลือกตารางที่ต้องการแสดง ถ้าในกรณีที่ต้องการทำนายเปปไทด์ชุดใหม่สามารถใส่ เปปไทด์ชุดใหม่ลงไปในส่วน input และกด Submit เพื่อทำนายอีกครั้ง
